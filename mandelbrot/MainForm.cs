﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractalus
{
    public partial class FractalusMainForm : Form
    {
        private System.Windows.Forms.Timer scrRefresh = new System.Windows.Forms.Timer();
        private System.Windows.Forms.Timer rndInitDisposer = new System.Windows.Forms.Timer();

        private Bitmap Renderedimage;
        private Color[,] ColorBuffer;

        private Color CalcColor = Color.FromArgb(255, 0, 255);
        private Color ClrColor = Color.FromArgb(0, 0, 0);

        private int CurrentPalette = 0;
        private float CurrentProgress = 100.0f;

        private bool IsRendering = false;
        private bool IsRefreshing = false;

        private decimal Fract_X = 0.0m;
        private decimal Fract_Y = 0.0m;
        private decimal Fract_Scale = 2m;
        private int Fract_maxI = 100;
        private float Fract_maxZ = 16.0f;

        Task RenderInit;

        public FractalusMainForm()
        {
            InitializeComponent();
            
            FetchBuffer();
            ClearBuffer();

            scrRefresh.Enabled = true;
            scrRefresh.Interval = 20;
            scrRefresh.Tick += RefreshCanvas;

            rndInitDisposer.Enabled = true;
            rndInitDisposer.Interval = 1;
            rndInitDisposer.Tick += CheckRenderInitStatus;

            try
            {
                RenderInit = new Task(() => Render());
                RenderInit.Start();
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Сработал вылет рендеринга при начале работы!");
            }
        }

        private void FetchBuffer()
        {
            Renderedimage = new Bitmap(Canvas.Width, Canvas.Height);
        }
        private void ClearBuffer()
        {
            try
            {
                for (int x = 0; x < Canvas.Width; x++)
                    for (int y = 0; y < Canvas.Height; y++)
                        Renderedimage.SetPixel(x, y, ClrColor);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Ошибка очистки буффера!");
            }
        }
        
        private void Render()
        {
            if (!IsRendering)
            {
                ColorBuffer = new Color[Canvas.Width, Canvas.Height];
                FetchBuffer();
                
                StatusBar_currentstat.Text = "Рендеринг...";

                IsRendering = true;

                var renderer = Parallel.For(0, Canvas.Width, (cX) =>
                {
                    for (int cY = 0; cY < Canvas.Height; cY++)
                        ColorBuffer[cX, cY] = ConvertI_ToColor(
                            FractalPoint(
                                (cX * Fract_Scale) + (Fract_X - (Canvas.Width * (Fract_Scale / 2))),
                                (cY * Fract_Scale) + (Fract_Y - (Canvas.Height * (Fract_Scale / 2))),
                                Fract_maxZ, Fract_maxI
                            ), Fract_maxI - 2
                        );
                });

                while (!renderer.IsCompleted && IsRefreshing)
                {
                    Thread.Sleep(50);
                }

                IsRendering = false;
                
                for (int _x = 0; _x < Canvas.Width; _x++)
                    for (int _y = 0; _y < Canvas.Height; _y++)
                        Renderedimage.SetPixel(_x, _y, ColorBuffer[_x, _y]);

                Canvas.Image = Renderedimage;

                StatusBar_currentstat.Text = "Готово!";
            }
        }

        public int FractalPoint(decimal x, decimal y, float ZLimit, int MaxIterations)
        {
            Complex z = new Complex(0, 0);
            int i;

            for (i = 0; (i < MaxIterations) && (z.Abs() < (decimal)ZLimit); i++)
                //z = z * z + new Complex(x / 180.0m, y / 180.0m);
                z = z * z + new Complex(x, y);

            return i;
        }

        private Color ConvertI_ToColor(int i, int limit = 498)
        {
            if (i > limit) return ClrColor;
            
            switch (CurrentPalette)
            {
                case 0:
                    if (i < (limit / 4.0f))
                        return Color.FromArgb(255, (int)(i / (limit / 4.0f) * 255), 0);
                    if (i < (limit / 2.0f))
                        return Color.FromArgb((int)(255 - ((i - (limit / 4.0f)) / (limit / 4.0f) * 255)), 255, 0);
                    if (i < ((3 * limit) / 4.0f))
                        return Color.FromArgb(0, 255, (int)((i - (limit / 2.0f)) / (limit / 4.0f) * 255));
                    else
                        return Color.FromArgb(0, (int)(255 - ((i - ((3 * limit) / 4.0f)) / (limit / 4.0f) * 255)), 255);

                default:
                    return Color.FromArgb(0, 0, 0);
            }

            return Color.FromArgb(0, 0, 0);
        }

        private void SelectSpectrum(object sender, EventArgs e)
        {
            MenuStrip_drawing_palette_spectrum.Checked      = false;
            MenuStrip_drawing_palette_blue.Checked          = false;
            MenuStrip_drawing_palette_grayscale.Checked     = false;
            MenuStrip_drawing_palette_retro.Checked         = false;
            MenuStrip_drawing_palette_monochrome.Checked    = false;
            MenuStrip_drawing_palette_gasfire.Checked       = false;
            MenuStrip_drawing_palette_candlefire.Checked    = false;

            ((ToolStripMenuItem)sender).Checked = true;

            if (MenuStrip_drawing_palette_spectrum.Checked) CurrentPalette = 0;
            if (MenuStrip_drawing_palette_blue.Checked) CurrentPalette = 1;
            if (MenuStrip_drawing_palette_grayscale.Checked) CurrentPalette = 2;
            if (MenuStrip_drawing_palette_retro.Checked) CurrentPalette = 3;
            if (MenuStrip_drawing_palette_monochrome.Checked) CurrentPalette = 4;
            if (MenuStrip_drawing_palette_gasfire.Checked) CurrentPalette = 5;
            if (MenuStrip_drawing_palette_candlefire.Checked) CurrentPalette = 6;
        }

        private void MenuCheck(object sender, EventArgs e)
        {
            ((ToolStripMenuItem)sender).Checked ^= true;
        }

        private void RefreshCanvas(object sender, EventArgs e)
        {
            try
            {
                if (IsRendering)
                {
                    IsRefreshing = true;

                    for (int _x = 0; _x < Canvas.Width; _x++)
                        for (int _y = 0; _y < Canvas.Height; _y++)
                            Renderedimage.SetPixel(_x, _y, ColorBuffer[_x, _y]);

                    Canvas.Image = Renderedimage;

                    IsRefreshing = false;
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show($"Ошибка копирования изображения! {ex.Source}");
                IsRefreshing = false;
            }
        }

        private void CheckRenderInitStatus(object sender, EventArgs e)
        {
            try
            {
                if (RenderInit.Status > (TaskStatus)4)
                    RenderInit.Dispose();
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Ошибка удаления потока для рендеринга!");
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            filler.Text = $"Позиция мыши: X: {(e.X * Fract_Scale) + (Fract_X - (Canvas.Width * (Fract_Scale / 2)))};" +
                $" Y: {(e.Y * Fract_Scale) + (Fract_Y - (Canvas.Height * (Fract_Scale / 2)))}. " +
                $"Масштаб: {Fract_Scale}";
        }

        private void Canvas_DoubleClick(object sender, EventArgs e)
        {
            if (!IsRendering)
            {
                Fract_X = (((MouseEventArgs)e).X * Fract_Scale) + (Fract_X - (Canvas.Width * (Fract_Scale / 2)));
                Fract_Y = (((MouseEventArgs)e).Y * Fract_Scale) + (Fract_Y - (Canvas.Height * (Fract_Scale / 2)));

                try
                {
                    RenderInit = new Task(() => Render());
                    RenderInit.Start();
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Сработал вылет рендеринга при перемещении по координатам!");
                }
            }
        }

        private void FractalusMainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (!IsRendering)
            {
                switch (e.KeyCode)
                {
                    case Keys.Add:
                        Fract_Scale /= Convert.ToDecimal(Math.Sqrt(2));
                        break;
                    case Keys.Subtract:
                        Fract_Scale *= Convert.ToDecimal(Math.Sqrt(2));
                        break;
                }

                if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract)
                {
                    try
                    {
                        RenderInit = new Task(() => Render());
                        RenderInit.Start();
                    }
                    catch(InvalidOperationException)
                    {
                        MessageBox.Show("Сработал вылет рендеринга при изменении масшатаба!");
                    }
                }
            }
        }
    }
}
