﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractalus
{
    /// <summary>
    /// Представление комплексного числа без явной мнимой части. Работает с decimal для высокой точности рассчётов.
    /// </summary>
    public class Complex
    {
        private decimal r, i;

        /// <summary>
        /// Генерирует число вида Z = R + Ii. Где i - мнимая единица.
        /// </summary>
        /// <param name="r">Действительный коэффициент комплексного числа.</param>
        /// <param name="i">Мнимый коэффициент комплексного числа.</param>
        public Complex(decimal r, decimal i)
        {
            this.r = r;
            this.i = i;
        }

        /// <summary>
        /// Сложение двух комплексных чисел.
        /// </summary>
        /// <returns>Возвращает новое комплексное число в виде результата суммы коэффициентов.</returns>
        public static Complex operator +(Complex rf, Complex c)
        {
            return new Complex(rf.r + c.r, rf.i + c.i);
        }
        /// <summary>
        /// Произведение двух комплексных чисел.
        /// </summary>
        /// <returns>Возвращает новое комплексное число в виде результата произведения комплексных чисел по формуле (R1 + I1i)*(R2 + I2i).</returns>
        public static Complex operator *(Complex rf, Complex c)
        {
            return new Complex(rf.r * c.r - rf.i * c.i, 2 * rf.r * c.i);
        }

        /// <summary>
        /// Модуль коэффициентов комплексного числа.
        /// </summary>
        /// <returns>Возвращает десятичное число.</returns>
        public decimal Abs()
        {
            return r * r + i * i;
        }
    }
}
