﻿namespace Fractalus
{
    partial class FractalusMainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuStrip_main = new System.Windows.Forms.MenuStrip();
            this.MenuStrip_options = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_options_watermark = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_options_multicore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_options_settings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_options_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_drawing = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_render = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_tofile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_drawing_coords = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_limits = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_drawing_palette = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_spectrum = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_blue = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_grayscale = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_retro = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_monochrome = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_gasfire = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_drawing_palette_candlefire = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_drawing_active = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_about = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuStrip_about_manual = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuStrip_about_program = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.StatusBar_currentstat = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBar_speed = new System.Windows.Forms.ToolStripStatusLabel();
            this.filler = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBar_progress = new System.Windows.Forms.ToolStripProgressBar();
            this.Canvas = new System.Windows.Forms.PictureBox();
            this.MenuStrip_main.SuspendLayout();
            this.StatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuStrip_main
            // 
            this.MenuStrip_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_options,
            this.MenuStrip_drawing,
            this.MenuStrip_about});
            this.MenuStrip_main.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip_main.Name = "MenuStrip_main";
            this.MenuStrip_main.Size = new System.Drawing.Size(800, 24);
            this.MenuStrip_main.TabIndex = 1;
            this.MenuStrip_main.Text = "menuStrip2";
            // 
            // MenuStrip_options
            // 
            this.MenuStrip_options.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_options_watermark,
            this.MenuStrip_options_multicore,
            this.toolStripMenuItem3,
            this.MenuStrip_options_settings,
            this.toolStripMenuItem7,
            this.MenuStrip_options_exit,
            this.toolStripMenuItem8});
            this.MenuStrip_options.Name = "MenuStrip_options";
            this.MenuStrip_options.Size = new System.Drawing.Size(56, 20);
            this.MenuStrip_options.Text = "Опции";
            // 
            // MenuStrip_options_watermark
            // 
            this.MenuStrip_options_watermark.Checked = true;
            this.MenuStrip_options_watermark.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuStrip_options_watermark.Name = "MenuStrip_options_watermark";
            this.MenuStrip_options_watermark.Size = new System.Drawing.Size(220, 22);
            this.MenuStrip_options_watermark.Text = "Водяной знак";
            this.MenuStrip_options_watermark.Click += new System.EventHandler(this.MenuCheck);
            // 
            // MenuStrip_options_multicore
            // 
            this.MenuStrip_options_multicore.Checked = true;
            this.MenuStrip_options_multicore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuStrip_options_multicore.Name = "MenuStrip_options_multicore";
            this.MenuStrip_options_multicore.Size = new System.Drawing.Size(220, 22);
            this.MenuStrip_options_multicore.Text = "Многоядерный рендеринг";
            this.MenuStrip_options_multicore.Click += new System.EventHandler(this.MenuCheck);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(217, 6);
            // 
            // MenuStrip_options_settings
            // 
            this.MenuStrip_options_settings.Name = "MenuStrip_options_settings";
            this.MenuStrip_options_settings.Size = new System.Drawing.Size(220, 22);
            this.MenuStrip_options_settings.Text = "Параметры";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(217, 6);
            // 
            // MenuStrip_options_exit
            // 
            this.MenuStrip_options_exit.Name = "MenuStrip_options_exit";
            this.MenuStrip_options_exit.Size = new System.Drawing.Size(220, 22);
            this.MenuStrip_options_exit.Text = "Выход";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(217, 6);
            // 
            // MenuStrip_drawing
            // 
            this.MenuStrip_drawing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_drawing_render,
            this.MenuStrip_drawing_tofile,
            this.toolStripMenuItem4,
            this.MenuStrip_drawing_coords,
            this.MenuStrip_drawing_limits,
            this.toolStripMenuItem1,
            this.MenuStrip_drawing_palette,
            this.MenuStrip_drawing_active,
            this.toolStripMenuItem5});
            this.MenuStrip_drawing.Name = "MenuStrip_drawing";
            this.MenuStrip_drawing.Size = new System.Drawing.Size(78, 20);
            this.MenuStrip_drawing.Text = "Отрисовка";
            // 
            // MenuStrip_drawing_render
            // 
            this.MenuStrip_drawing_render.Name = "MenuStrip_drawing_render";
            this.MenuStrip_drawing_render.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_render.Text = "Рендерить";
            // 
            // MenuStrip_drawing_tofile
            // 
            this.MenuStrip_drawing_tofile.Name = "MenuStrip_drawing_tofile";
            this.MenuStrip_drawing_tofile.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_tofile.Text = "Рендерить в файл";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(277, 6);
            // 
            // MenuStrip_drawing_coords
            // 
            this.MenuStrip_drawing_coords.Name = "MenuStrip_drawing_coords";
            this.MenuStrip_drawing_coords.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_coords.Text = "Указать координаты";
            // 
            // MenuStrip_drawing_limits
            // 
            this.MenuStrip_drawing_limits.Name = "MenuStrip_drawing_limits";
            this.MenuStrip_drawing_limits.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_limits.Text = "Лимиты вычислений";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(277, 6);
            // 
            // MenuStrip_drawing_palette
            // 
            this.MenuStrip_drawing_palette.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_drawing_palette_spectrum,
            this.MenuStrip_drawing_palette_blue,
            this.MenuStrip_drawing_palette_grayscale,
            this.MenuStrip_drawing_palette_retro,
            this.MenuStrip_drawing_palette_monochrome,
            this.MenuStrip_drawing_palette_gasfire,
            this.MenuStrip_drawing_palette_candlefire,
            this.toolStripMenuItem9});
            this.MenuStrip_drawing_palette.Name = "MenuStrip_drawing_palette";
            this.MenuStrip_drawing_palette.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_palette.Text = "Палитра";
            // 
            // MenuStrip_drawing_palette_spectrum
            // 
            this.MenuStrip_drawing_palette_spectrum.Checked = true;
            this.MenuStrip_drawing_palette_spectrum.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuStrip_drawing_palette_spectrum.Name = "MenuStrip_drawing_palette_spectrum";
            this.MenuStrip_drawing_palette_spectrum.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_spectrum.Text = "Спектр (по умолчанию)";
            this.MenuStrip_drawing_palette_spectrum.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_blue
            // 
            this.MenuStrip_drawing_palette_blue.Name = "MenuStrip_drawing_palette_blue";
            this.MenuStrip_drawing_palette_blue.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_blue.Text = "Синяя";
            this.MenuStrip_drawing_palette_blue.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_grayscale
            // 
            this.MenuStrip_drawing_palette_grayscale.Name = "MenuStrip_drawing_palette_grayscale";
            this.MenuStrip_drawing_palette_grayscale.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_grayscale.Text = "Серая";
            this.MenuStrip_drawing_palette_grayscale.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_retro
            // 
            this.MenuStrip_drawing_palette_retro.Name = "MenuStrip_drawing_palette_retro";
            this.MenuStrip_drawing_palette_retro.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_retro.Text = "Ретро";
            this.MenuStrip_drawing_palette_retro.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_monochrome
            // 
            this.MenuStrip_drawing_palette_monochrome.Name = "MenuStrip_drawing_palette_monochrome";
            this.MenuStrip_drawing_palette_monochrome.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_monochrome.Text = "Монохром";
            this.MenuStrip_drawing_palette_monochrome.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_gasfire
            // 
            this.MenuStrip_drawing_palette_gasfire.Name = "MenuStrip_drawing_palette_gasfire";
            this.MenuStrip_drawing_palette_gasfire.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_gasfire.Text = "Голубые огни";
            this.MenuStrip_drawing_palette_gasfire.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // MenuStrip_drawing_palette_candlefire
            // 
            this.MenuStrip_drawing_palette_candlefire.Name = "MenuStrip_drawing_palette_candlefire";
            this.MenuStrip_drawing_palette_candlefire.Size = new System.Drawing.Size(207, 22);
            this.MenuStrip_drawing_palette_candlefire.Text = "Свеча";
            this.MenuStrip_drawing_palette_candlefire.Click += new System.EventHandler(this.SelectSpectrum);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(204, 6);
            // 
            // MenuStrip_drawing_active
            // 
            this.MenuStrip_drawing_active.Checked = true;
            this.MenuStrip_drawing_active.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuStrip_drawing_active.Name = "MenuStrip_drawing_active";
            this.MenuStrip_drawing_active.Size = new System.Drawing.Size(280, 22);
            this.MenuStrip_drawing_active.Text = "Отображать текущие точки просчёта";
            this.MenuStrip_drawing_active.Click += new System.EventHandler(this.MenuCheck);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(277, 6);
            // 
            // MenuStrip_about
            // 
            this.MenuStrip_about.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStrip_about_manual,
            this.toolStripMenuItem2,
            this.MenuStrip_about_program,
            this.toolStripMenuItem6});
            this.MenuStrip_about.Name = "MenuStrip_about";
            this.MenuStrip_about.Size = new System.Drawing.Size(65, 20);
            this.MenuStrip_about.Text = "Справка";
            // 
            // MenuStrip_about_manual
            // 
            this.MenuStrip_about_manual.Name = "MenuStrip_about_manual";
            this.MenuStrip_about_manual.Size = new System.Drawing.Size(149, 22);
            this.MenuStrip_about_manual.Text = "Инструкции";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(146, 6);
            // 
            // MenuStrip_about_program
            // 
            this.MenuStrip_about_program.Name = "MenuStrip_about_program";
            this.MenuStrip_about_program.Size = new System.Drawing.Size(149, 22);
            this.MenuStrip_about_program.Text = "О программе";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(146, 6);
            // 
            // StatusBar
            // 
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBar_currentstat,
            this.StatusBar_speed,
            this.filler,
            this.StatusBar_progress});
            this.StatusBar.Location = new System.Drawing.Point(0, 624);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.StatusBar.Size = new System.Drawing.Size(800, 24);
            this.StatusBar.TabIndex = 2;
            this.StatusBar.Text = "statusStrip1";
            // 
            // StatusBar_currentstat
            // 
            this.StatusBar_currentstat.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.StatusBar_currentstat.Name = "StatusBar_currentstat";
            this.StatusBar_currentstat.Size = new System.Drawing.Size(77, 19);
            this.StatusBar_currentstat.Text = "Ожидание...";
            // 
            // StatusBar_speed
            // 
            this.StatusBar_speed.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.StatusBar_speed.Name = "StatusBar_speed";
            this.StatusBar_speed.Size = new System.Drawing.Size(160, 19);
            this.StatusBar_speed.Text = "Скорость рассчётов: -- т./с";
            // 
            // filler
            // 
            this.filler.Name = "filler";
            this.filler.Size = new System.Drawing.Size(281, 19);
            this.filler.Spring = true;
            // 
            // StatusBar_progress
            // 
            this.StatusBar_progress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.StatusBar_progress.Margin = new System.Windows.Forms.Padding(1, 3, 10, 3);
            this.StatusBar_progress.Maximum = 1000;
            this.StatusBar_progress.Name = "StatusBar_progress";
            this.StatusBar_progress.Size = new System.Drawing.Size(256, 18);
            this.StatusBar_progress.Step = 1;
            this.StatusBar_progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // Canvas
            // 
            this.Canvas.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Canvas.Location = new System.Drawing.Point(0, 24);
            this.Canvas.Name = "Canvas";
            this.Canvas.Size = new System.Drawing.Size(800, 600);
            this.Canvas.TabIndex = 3;
            this.Canvas.TabStop = false;
            this.Canvas.DoubleClick += new System.EventHandler(this.Canvas_DoubleClick);
            this.Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseMove);
            // 
            // FractalusMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 648);
            this.Controls.Add(this.Canvas);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.MenuStrip_main);
            this.Name = "FractalusMainForm";
            this.Text = "Fractalus";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FractalusMainForm_KeyDown);
            this.MenuStrip_main.ResumeLayout(false);
            this.MenuStrip_main.PerformLayout();
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStrip_main;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_options;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_options_watermark;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_options_multicore;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_coords;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_limits;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_about;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_about_manual;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_about_program;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_options_settings;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_options_exit;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_render;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_tofile;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_spectrum;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_blue;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_grayscale;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_retro;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_monochrome;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_gasfire;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_palette_candlefire;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem MenuStrip_drawing_active;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.ToolStripStatusLabel StatusBar_currentstat;
        private System.Windows.Forms.ToolStripStatusLabel StatusBar_speed;
        private System.Windows.Forms.ToolStripProgressBar StatusBar_progress;
        private System.Windows.Forms.ToolStripStatusLabel filler;
    }
}

